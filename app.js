function konversiMenit(time) {
  // you can only write your code here!
  var minutes = Math.floor(time / 60);
  var seconds = time - minutes * 60;
  return (minutes+':'+seconds)
}

// TEST CASES
console.log(konversiMenit(63)); // 1:03
console.log(konversiMenit(124)); // 2:04
console.log(konversiMenit(53)); // 0:53
console.log(konversiMenit(88)); // 1:28
console.log(konversiMenit(120)); // 2:00